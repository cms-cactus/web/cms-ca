These are the commands to be performed when you want to use the CA to generate
and sign a new key pair for your host.

You need to retrieve the CMS_L1_Software_CA.key (stored in the usual place).
Then you need to run the script

```
./createNewKeypair.sh
```

You are requested to 
*  type the desired hostname ( e.g. hostname.cern.ch ) or keep the default one (l1ce.cms)
*  enter the pass phrase for CA.key

The keys are created in the `generated-pairs` directory.

The script uses `openssl-template.cnf` to generate the new `openssl.cnf`
which inserts, among others attributes, the SubjectAltName (SAN) in the certificate.
SAN is strictly requested by chrome>58.

It's a good practice to check that there are no significant changes among the generated `openssl.cnf` and the default `openssl.cnf`.