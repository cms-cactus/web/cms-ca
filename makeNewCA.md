<!-- https://datacenteroverlords.com/2012/03/01/creating-your-own-ssl-certificate-authority/ -->

This creates a new CA. This is only shown for educational purposes, for people who also want a CA for some weird custom domain.

```bash
openssl genrsa -des3 -out CMS_L1_Software_CA.key 4096
# Enter pass phrase for rootCA.key:
# Verifying - Enter pass phrase for rootCA.key:
openssl req -x509 -new -nodes -key CMS_L1_Software_CA.key -sha512 -days 36525 -out CMS_L1_Software_CA.crt -multivalue-rdn -subj "/DC=cms/CN=CERN CMS Level-1 Online Software Certification Authority/C=ch/O=CERN"
```
