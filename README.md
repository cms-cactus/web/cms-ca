# CERN CMS Level-1 Software Certificate Authority
This CA is used to sign crypto key pairs for the `\*.cms` domain used at P5.

You need to install the CA on your system before it trusts any key pairs signed by it.

## installation
[download this file](https://gitlab.cern.ch/cactus/cms-ca/raw/master/CMS_L1_Software_CA.crt) and follow installation instructions for your browsers:

### Firefox
1. go to your settings -> advanced -> certificates
   ![](/docs/firefox1.png)
1. click view certificates -> authorities
   ![](/docs/firefox2.png)
1. click import, and select the certificate you downloaded
1. be sure to select 'trust for websites'
1. `CERN CMS Level-1 Online Software Certification Authority` should now appear in the list of authorities
   ![](/docs/firefox3.png)

### Google Chrome
#### Mac OS
1. go to [ssl settings](chrome://settings/search#ssl) and click `manage certificates`
   ![](/docs/chrome1.png)
2. click on certificates
   ![](/docs/keychain1.png)
3. drag in the downloaded certificate

   there should now be an entry `CERN CMS Level-1 Online Software Certification Authority`
   ![](/docs/keychain2.png)
4. double click the entry and set the dropdown to 'always trust'
   ![](/docs/keychain3.png)

#### Linux
1. go to [ssl settings](chrome://settings/search#ssl) and click `manage certificates`
   ![](/docs/chrome1.png)
2. go to `Authorities`, click `Import..`  and select the certificate you downloaded
   ![](/docs/chromeLinux1.png)
3. be sure to select 'trust for websites'
   ![](/docs/chromeLinux2.png)
4. `CERN CMS Level-1 Online Software Certification Authority` should now appear in the list of authorities

## (advanced) Signing new keys with the CMS CA
If you want to generate a keypair that is signed with this CA,
follow [these](create-new-keypair.md) instructions.

Note that you will need the CA's private key, which is in a secret place.
