#!/bin/sh

OLD_HOSTNAME="l1ce.cms"
read -p "Type the hostname: (l1ce.cms) " NEW_HOSTNAME
NEW_HOSTNAME=${NEW_HOSTNAME:-l1ce.cms}

if [ "${OLD_HOSTNAME}" != "${NEW_HOSTNAME}" ] ; then
    echo "replacing $OLD_HOSTNAME -> $NEW_HOSTNAME"
fi

sed "s/$OLD_HOSTNAME/$NEW_HOSTNAME/g" openssl-template.cnf > openssl.cnf

### create key
openssl genrsa -out generated-pairs/"${NEW_HOSTNAME}".key 4096
test "$?" -ne 0 && echo -e "\nERROR: \`openssl genrsa\` failed, exit script\n" && exit

echo "created generated-pairs/${NEW_HOSTNAME}.key"
### create certificate
openssl req -new -key generated-pairs/"${NEW_HOSTNAME}".key -out generated-pairs/"${NEW_HOSTNAME}".csr -config openssl.cnf 
test "$?" -ne 0 && echo -e "\nERROR: \`openssl req\` failed, exit script\n" && exit
echo "created generated-pairs/${NEW_HOSTNAME}.csr"

### dump certificate
openssl req -in generated-pairs/"${NEW_HOSTNAME}".csr -text ## OK

### sign certificate
openssl x509 -req -in generated-pairs/"${NEW_HOSTNAME}".csr -CA CMS_L1_Software_CA.crt -CAkey CMS_L1_Software_CA.key -CAcreateserial -out generated-pairs/"${NEW_HOSTNAME}".crt -days 36525 -sha512 -extensions req_ext -extfile openssl.cnf -passin file:CMS_L1_Software_CA.pwd
test "$?" -eq 0 || echo -e "\nERROR: \`openssl x509\` failed, exit script\n" && exit
echo "created generated-pairs/${NEW_HOSTNAME}.crt"


### dump signed certificate
openssl x509 -inform PEM -in generated-pairs/"${NEW_HOSTNAME}".crt -text
